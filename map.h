#ifndef MAP_H
#define MAP_H

#include <QGraphicsScene>
#include <QGraphicsView>

class Map
{
	public:
		enum Type { GRID = 2, /*CUBE = 3, */HYPERCUBE = 4 };
		enum IAMode { DUMB, SMART };

		explicit Map(qint32 const nodesCount, qint32 const policemenCount, Type const mapType, IAMode const iaMode);
		virtual ~Map();
		QList<qint32> const accessibleMoves(qint32 const node) const;
		QList<qint32> const availableMoves(qint32 const node) const;
		bool checkCapture() const;
		void moveThief();
		virtual void movePolicemen() = 0;
		virtual void draw(QGraphicsScene * scene) const = 0;

		inline qint32 getNodesCount() const
		{
			return _nodesCount;
		}
		inline qint32 getPolicemenCount() const
		{
			return _policemenCount;
		}/*
		inline qint32 const * getPositions() const
		{
			return _positions;
		}
		inline qint32 getPosition(qint32 const index) const
		{
			return _positions[index];
		}*/
		inline Map::Type getMapType() const
		{
			return _mapType;
		}

	protected:
		bool **		_graph;
		qint32		_nodesCount;
		qint32		_policemenCount;
		qint32 *	_positions;
		Type		_mapType;
		IAMode		_iaMode;

		inline qint32 & thiefPosition() const
		{
			return _positions[0];
		}
		inline qint32 & policemanPosition(qint32 const index) const
		{
			return _positions[index+1];
		}
		inline virtual qreal pos2x(qint32 const position) const = 0;
		inline virtual qreal pos2y(qint32 const position) const = 0;
};

#endif // MAP_H
