===========================
========== CMIYC ==========
=== Catch Me If You Can ===
===========================

This project is a work for the Algorithm course of 3rd year at Polytech Lyon.
The aim is to catch a thief with a minimal number of policemen, and a minimal number of steps.
Differents kinds of maps must be handled :
- a grid
- a torus
- a cube
- an hypercube

Moves
Each character can only move along an edge.
The thief starts to move.
Policemen moves one by one (only one can move when it is their turn to play)
