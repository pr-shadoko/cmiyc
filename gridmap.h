#ifndef GRIDMAP_H
#define GRIDMAP_H

#include <QtGlobal>

#include <map.h>

class GridMap : public Map
{
	public:
		explicit GridMap(qint32 const width, qint32 const height, IAMode const iaMode);
		~GridMap();
		void draw(QGraphicsScene * scene) const;
		void movePolicemen();

		inline qint32 width() const
		{
			return _width;
		}
		inline qint32 height() const
		{
			return _height;
		}

	protected:
		inline qreal pos2x(qint32 const position) const
		{
			return position % _width;
		}
		inline qreal pos2y(qint32 const position) const
		{
			return position / _width;
		}

	private:
		qint32 _width;
		qint32 _height;

};

#endif // GRIDMAP_H
