#ifndef HYPERCUBICMAP_H
#define HYPERCUBICMAP_H

#include <QtGlobal>
#include <QtCore/qmath.h>

#include <map.h>

class HypercubicMap : public Map
{
	public:
		explicit HypercubicMap(IAMode const iaMode = SMART);
		~HypercubicMap();
		void draw(QGraphicsScene * scene) const;
		void movePolicemen();

	protected:
		qint32 *	_targets;

		inline qint32 distance(qint32 i, qint32 j) const
		{

			qint32 diff = i ^ j;
			qint32 diffCount = 0;
			// Count the number of differences between i and j
			while(diff != 0)
			{
				diffCount += (diff & static_cast<qint32>(0x1));
				diff >>= 1;
			}
			return diffCount;
		}
		inline qreal pos2x(qint32 const position) const
		{
			qreal x(0);
			x += (position & static_cast<qint32>(0x2)) ? 1 : 0;
			x += (position & static_cast<qint32>(0x4)) ? 1/sqrt(2) : 0;
			x += (position & static_cast<qint32>(0x8)) ? -1/sqrt(2) : 0;
			return x;
		}
		inline qreal pos2y(qint32 const position) const
		{
			qreal y(0);
			y += (position & static_cast<qint32>(0x1)) ? 1 : 0;
			y += (position & static_cast<qint32>(0x4)) ? 1/sqrt(2) : 0;
			y += (position & static_cast<qint32>(0x8)) ? 1/sqrt(2) : 0;
			return y;
		}
		void chooseTargets();
};

#endif // HYPERCUBICMAP_H
