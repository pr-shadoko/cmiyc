#include "map.h"

Map::Map(qint32 const nodesCount, qint32 const policemenCount, Type const mapType, IAMode const iaMode) :
	_nodesCount(nodesCount),
	_policemenCount(policemenCount),
	_mapType(mapType),
	_iaMode(iaMode)
{
	_graph = new bool*[_nodesCount];
	for(qint32 i=0; i<_nodesCount; ++i)
	{
		_graph[i] = new bool[_nodesCount];
		for(qint32 j=0; j<_nodesCount; ++j)
		{
			_graph[i][j] = false;
		}
	}
	_positions = new qint32[_policemenCount+1];


	// Liste des cases libres
	QList<qint32> choixPossibles;
	for(qint32 i=0; i<_nodesCount; i++)
	{
		choixPossibles.append(i);
	}
	// Positionner gendarmes et voleur
	for(qint32 i=0; i<=_policemenCount; ++i)
	{
		qint32 indice = qrand() % choixPossibles.size();
		_positions[i] = choixPossibles[indice];
		choixPossibles.removeAt(indice);
	}
}

Map::~Map()
{
	for(qint32 i=0; i<_nodesCount; ++i)
	{
		delete[] _graph[i];
	}
	delete[] _graph;
	delete[] _positions;
}

QList<qint32> const Map::accessibleMoves(qint32 const node) const
{
	QList<qint32> moves;

	for(qint32 i=0; i<_nodesCount; ++i)
	{
		if(_graph[node][i])
		{
			moves.append(i);
		}
	}
	return moves;
}

QList<qint32> const Map::availableMoves(qint32 const node) const
{
	QList<qint32> allMoves = accessibleMoves(node);
	QList<qint32> moves = allMoves;

	// On retire des déplacements autorisés les positions des gendarmes et adjacentes
	for(qint32 i=0; i<_policemenCount; ++i)
	{
		moves.removeOne(policemanPosition(i));
		for(qint32 j=0; j<_nodesCount; ++j)
		{
			if(_graph[policemanPosition(i)][j])
			{
				moves.removeOne(j);
			}
		}
	}

	return moves.isEmpty() ? allMoves : moves;
}

bool Map::checkCapture() const
{
	for(qint32 i=0; i<_policemenCount; ++i)
	{
		if(thiefPosition() == policemanPosition(i))
		{
			return true;
		}
	}
	return false;
}

void Map::moveThief()
{
	QList<qint32> moves;
	switch(_iaMode)
	{
		case DUMB:
		{
			moves = accessibleMoves(thiefPosition());
			break;
		}

		case SMART:
		{
			moves = availableMoves(thiefPosition());
			break;
		}
	}
	thiefPosition() = moves[qrand() % moves.size()];
}
