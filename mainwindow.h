#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QProgressBar>

#include <gamecontroller.h>
#include <newgamedialog.h>

namespace Ui {
	class MainWindow;
	class NewGameDialog;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		static int const BENCHMARK_COUNT;

		explicit MainWindow(QWidget * parent = NULL);
		~MainWindow();

	signals:
		void resized();

	public slots:
		void newGame();
		void endGame();

	protected:
		virtual void resizeEvent(QResizeEvent *);

	private:
		Ui::MainWindow *	_ui;
		GameController *	_gc;
		bool				_benchmark;
		QProgressBar *		_benchmarkProgress;
		qint32				_benchmarkMin;
		qint32				_benchmarkMax;
		qreal				_benchmarkMean;
};

#endif // MAINWINDOW_H
