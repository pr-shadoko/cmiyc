#include "gamecontroller.h"

qint32 const GameController::MAXSTEP = 100;

GameController::GameController(Map::Type const mapType, Map::IAMode const iaMode, QObject * parent) :
	QObject(parent),
	_step(0)
{
	_scene = new QGraphicsScene();

	switch(mapType)
	{
		default:
		case Map::GRID:
		{
			_map = new GridMap(8, 8, iaMode);
			break;
		}

		case Map::HYPERCUBE:
		{
			_map = new HypercubicMap(iaMode);
			break;
		}
	}
}

GameController::~GameController()
{
	delete _scene;
	delete _map;
}

//	TODO: Multithreading ? Lorsqu'on ferme la fenêtre, la boucle continue...
void GameController::autoPlay()
{
	for(_step=0; _step < MAXSTEP;)// _step incrémenté dans nextStep()
	{
		if(nextStep())
		{
			return;
		}
	}
}

bool GameController::nextStep()
{
	_step++;
	_map->moveThief();
	update();
	if(_map->checkCapture())
	{
		emit gameOver();
		return true;
	}


	_map->movePolicemen();
	update();
	if(_map->checkCapture())
	{
		emit gameOver();
		return true;
	}
	return false;
}

void GameController::update()
{
	_map->draw(_scene);
}
