#ifndef NEWGAMEDIALOG_H
#define NEWGAMEDIALOG_H

#include <QDialog>

#include <map.h>

namespace Ui {
	class NewGameDialog;
	}

class NewGameDialog : public QDialog
{
		Q_OBJECT

	public:
		explicit NewGameDialog(QWidget * parent = NULL);
		~NewGameDialog();
		Map::Type getSelectedMap() const;
		bool getBenchmark() const;
		Map::IAMode getThiefIA() const;

	private slots:
		void on__button_grid8x8_clicked(bool checked);
		void on__button_hypercube_clicked(bool checked);
		void on__button_benchmark_toggled(bool checked);
		void on__button_thief_ia_toggled(bool checked);

	private:
		Ui::NewGameDialog * _ui;
};

#endif // DIALOG_H
