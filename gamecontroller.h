#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QCoreApplication>
#include <QTime>

#include <gridmap.h>
#include <hypercubicmap.h>

class GameController : public QObject
{
	Q_OBJECT

	public:
		static qint32 const	MAXSTEP;

		explicit GameController(Map::Type const mapType, Map::IAMode const iaMode, QObject * parent = NULL);
		~GameController();

		inline QGraphicsScene * getScene() const
		{
			return _scene;
		}
		inline qint32 getStep() const
		{
			return _step;
		}

	signals:
		void gameOver();

	public slots:
		bool nextStep();
		void autoPlay();
		void update();

	private:
		QGraphicsScene *	_scene;
		Map::IAMode			_iaMode;
		Map *				_map;
		qint32				_step;
};

#endif // GAMECONTROLLER_H
