#include "newgamedialog.h"
#include "ui_newgamedialog.h"

NewGameDialog::NewGameDialog(QWidget * parent) :
	QDialog(parent),
	_ui(new Ui::NewGameDialog)
{
	_ui->setupUi(this);
	_ui->_button_box->button(QDialogButtonBox::Ok)->setDefault(true);
}

NewGameDialog::~NewGameDialog()
{
	delete _ui;
}

Map::Type NewGameDialog::getSelectedMap() const
{
	if(_ui->_button_hypercube->isChecked())
	{
		return Map::HYPERCUBE;
	}

	return Map::GRID;
}

bool NewGameDialog::getBenchmark() const
{
	return _ui->_button_benchmark->isChecked();
}

Map::IAMode NewGameDialog::getThiefIA() const
{
	return _ui->_button_thief_ia->isChecked() ? Map::SMART : Map::DUMB;
}

void NewGameDialog::on__button_grid8x8_clicked(bool checked)
{
	if(checked)
	{
		_ui->_button_hypercube->setChecked(false);
	}
	else
	{
		_ui->_button_grid8x8->setChecked(true);
	}
}

void NewGameDialog::on__button_hypercube_clicked(bool checked)
{
	if(checked)
	{
		_ui->_button_grid8x8->setChecked(false);
	}
	else
	{
		_ui->_button_hypercube->setChecked(true);
	}
}


void NewGameDialog::on__button_benchmark_toggled(bool checked)
{
	if(checked)
	{
		_ui->_button_benchmark->setText(tr("Benchmark : On"));
	}
	else
	{
		_ui->_button_benchmark->setText(tr("Benchmark : Off"));
	}
}

void NewGameDialog::on__button_thief_ia_toggled(bool checked)
{
	if(checked)
	{
		_ui->_button_thief_ia->setText(tr("IA Voleur : Prudent"));
	}
	else
	{
		_ui->_button_thief_ia->setText(tr("IA Voleur : Aléatoire"));
	}
}
