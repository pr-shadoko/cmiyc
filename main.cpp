#include <ctime>
#include <QtGui/QApplication>
#include <QTextCodec>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
	qsrand(time(NULL));
	QTextCodec * textCodec = QTextCodec::codecForName("UTF-8");
	QTextCodec::setCodecForCStrings(textCodec);
	QTextCodec::setCodecForTr(textCodec);
	QApplication a(argc, argv);
	MainWindow w;
	w.show();

	return a.exec();
}
