#include "gridmap.h"

GridMap::GridMap(qint32 const width, qint32 const height, IAMode const iaMode) :
	Map(width * height, 2, GRID, iaMode),
	_width(width),
	_height(height)
{
	for(qint32 i=0; i<_nodesCount; ++i)
	{
		// Link to the left
		if((i % _width) != 0)
		{
			_graph[i][i-1] = true;
		}

		// Link to the right
		if((i % _width) != _width-1)
		{
			_graph[i][i+1] = true;
		}

		// Link to the top
		if(i >= _width)
		{
			_graph[i-_width][i] = true;
		}

		// Link to the bottom
		if(i < _width * (_height - 1))
		{
			_graph[i+_width][i] = true;
		}
	}
}

GridMap::~GridMap()
{
}

void GridMap::draw(QGraphicsScene * scene) const
{
	scene->clear();
	scene->setBackgroundBrush(Qt::black);
	QPen boardPen(Qt::cyan);
	QBrush thiefBrush(Qt::yellow);
	QBrush policemanBrush(Qt::blue);

	for(qint32 x=0; x<=_width; ++x)
	{
		scene->addLine(qreal(x), qreal(0), qreal(x), qreal(_height), boardPen);
	}
	for(qint32 y=0; y<=_height; ++y)
	{
		scene->addLine(qreal(0), qreal(y), qreal(_width), qreal(y), boardPen);
	}

	scene->addEllipse(pos2x(thiefPosition())+.05, pos2y(thiefPosition())+.05, qreal(.9), qreal(.9), QPen(), thiefBrush);
	for(qint32 i=0; i<getPolicemenCount(); ++i)
	{
		scene->addEllipse(pos2x(policemanPosition(i))+.15, pos2y(policemanPosition(i))+.15, qreal(.7), qreal(.7), QPen(), policemanBrush);
	}

	scene->views()[0]->fitInView(qreal(-1), qreal(-1), qreal(_width), qreal(_height), Qt::KeepAspectRatio);
}

void GridMap::movePolicemen()
{
	QList<QPoint> distances; // Contient les distances entre chaque policier et le voleur
	QList<qint32> proximite; // Contient les indices des policiers triés par distance au voleur

	// On classe les policiers par distance au voleur
	for(qint32 i=0; i<_policemenCount; ++i)
	{
		distances.append(QPoint(qAbs(pos2x(thiefPosition()) - pos2x(policemanPosition(i))),
								qAbs(pos2y(thiefPosition()) - pos2y(policemanPosition(i)))));
		QList<qint32>::iterator it = proximite.begin();

		while(it != proximite.end() && distances[i].manhattanLength() > distances[*it].manhattanLength())
		{
			++it;
		}
		proximite.insert(it, i);
	}

	int movingPoliceman = proximite[0];
	if(distances[movingPoliceman].manhattanLength() == 2)
	{
		movingPoliceman = proximite[1];
	}

	if(distances[movingPoliceman].x() > distances[movingPoliceman].y())
	{
		if(pos2x(thiefPosition()) - pos2x(policemanPosition(movingPoliceman)) > 0)
		{
			policemanPosition(movingPoliceman)++;
		}
		else
		{
			policemanPosition(movingPoliceman)--;
		}
	}
	else
	{
		if(pos2y(thiefPosition()) - pos2y(policemanPosition(movingPoliceman)) > 0)
		{
			policemanPosition(movingPoliceman) += _width;
		}
		else
		{
			policemanPosition(movingPoliceman) -= _width;
		}
	}
}
