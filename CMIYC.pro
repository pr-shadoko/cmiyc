#-------------------------------------------------
#
# Project created by QtCreator 2012-12-19T09:55:53
#
#-------------------------------------------------

########################################## Project

PROJECT_NAME	=	CMIYC
TARGET		=	$${PROJECT_NAME}
TEMPLATE	=	app
QT			+=	core gui

########################### Encoding Configuration

CODECFORTR	=	UTF-8
CODECFORSRC	=	UTF-8

################################# Compiler Options

QMAKE_CXXFLAGS			+=	-Wall -Winit-self -Wundef -Winline -Wold-style-cast -Wredundant-decls
QMAKE_CXXFLAGS_RELEASE	+=	-O2 -fomit-frame-pointer -s

###################################### Directories

ROOT_DIR	=	.

########################################## Sources

HEADERS		+=	$${ROOT_DIR}/*.h
SOURCES		+=	$${ROOT_DIR}/*.cpp
FORMS		+=	$${ROOT_DIR}/*.ui
OTHER_FILES	+=	$${ROOT_DIR}/README.txt

RESOURCES += \
	resources.qrc
