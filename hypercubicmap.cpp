#include "hypercubicmap.h"

HypercubicMap::HypercubicMap(IAMode const iaMode) :
	Map(qPow(2, static_cast<qint32>(HYPERCUBE)), 2, HYPERCUBE, iaMode)
{
	// i and j are indices of the nodes. Their binary representation gives coordinates :
	// Cube : (top/bottom, left/right, back/front).
	// Hypercube : (external/internal, top/bottom, left/right, back/front).
	for(qint32 i=0; i<_nodesCount; ++i)
	{
		for(qint32 j=0; j<_nodesCount; ++j)
		{
			if(i == j)
				continue;

			qint32 diffCount = distance(i, j);

			// If there is at least one different coordinate between the nodes, they are not connected.
			// So, if there is only one difference, they are connected.
			if(diffCount == 1)
				_graph[i][j] = true;
		}
	}
	_targets = new qint32[getPolicemenCount()];
	chooseTargets();
}

HypercubicMap::~HypercubicMap()
{
	delete[] _targets;
}

void HypercubicMap::chooseTargets()
{
	// Deux "couleurs" : Vrai et Faux
	bool thiefColor = false;
	bool * policemanColor = new bool[getPolicemenCount()];
	for(qint32 i=0; i<getPolicemenCount(); ++i)
	{
		// Vrai si la distance voleur-policier est impaire, Faux si elle est paire.
		policemanColor[i] = static_cast<bool>(distance(thiefPosition(), policemanPosition(i)) & 0x1);
	}

	// Si les gendarmes sont de couleurs différentes
	if(policemanColor[0] != policemanColor[1])
	{
//		qDebug("Gendarmes de couleurs différentes");
		// Placer le gendarme de la même couleur que le voleur à l'opposé de l'autre
		if(policemanColor[0] == thiefColor)
		{
			_targets[0] = policemanPosition(1) ^ 0xF;
			_targets[1] = policemanPosition(1);
		}
		else
		{
			_targets[0] = policemanPosition(0);
			_targets[1] = policemanPosition(0) ^ 0xF;
		}
	}
	else
	{
		/// Si les gendarmes sont de la même couleur que le voleur
		if(policemanColor[0] == thiefColor)
		{
//			qDebug("Gendarmes et voleur de la même couleur");
			// Placer un des gendarmes à l'opposé de l'autre
			_targets[0] = policemanPosition(1) ^ 0xF;
			_targets[1] = policemanPosition(1);
		}
		else
		{
//			qDebug("Voleur de couleur différente des gendarmes");
			// Bouger un gendarme d'une case, puis placer l'autre à l'opposé du premier
			// S'ils sont déjà à l'opposé l'un de l'autre
			if(distance(policemanPosition(0), policemanPosition(1)) == 4)
			{
				// Déplacer un gendarme d'une case sur n'importe quelle dimension
				_targets[0] = policemanPosition(0) ^ 0x1;
			}
			// Sinon, ils ont deux coordonnées communes
			else
			{
				// Supprimer une des coordonnées communes
				qint32 diff = policemanPosition(0) ^ policemanPosition(1);
				qint32 indexSame = 1;
				while(diff & indexSame)
				{
					indexSame <<= 1;
				}
				_targets[0] = policemanPosition(0) ^ indexSame;
			}
			// Placer l'autre gendarme à l'opposé du premier
			_targets[1] = _targets[0] ^ 0xF;
		}
	}

	delete[] policemanColor;
}

void HypercubicMap::draw(QGraphicsScene * scene) const
{
	scene->clear();
	scene->setBackgroundBrush(Qt::black);
	QPen boardPen(Qt::cyan);
	QBrush boardBrush(Qt::cyan);
	QBrush thiefBrush(Qt::yellow);
	QBrush policemanBrush(Qt::blue);


	for(qint32 node=0; node<_nodesCount; ++node)
	{
		scene->addEllipse(pos2x(node)-.1, pos2y(node)-.1, qreal(.2), qreal(.2), QPen(), boardBrush);
		for(qint32 node2=node+1; node2<_nodesCount; ++node2)
		{
			if(_graph[node][node2])
			{
				scene->addLine(pos2x(node), pos2y(node), pos2x(node2), pos2y(node2), boardPen);
			}
		}
	}
	scene->addEllipse(pos2x(thiefPosition())-.1, pos2y(thiefPosition())-.1, qreal(.2), qreal(.2), QPen(), thiefBrush);
	for(qint32 i=1; i<=_policemenCount; ++i)
	{
		scene->addEllipse(pos2x(_positions[i])-.075, pos2y(_positions[i])-.075, qreal(.15), qreal(.15), QPen(), policemanBrush);
	}

	scene->views()[0]->fitInView(qreal(-1), qreal(-1), qreal(1.3+sqrt(2)), qreal(1.3+sqrt(2)), Qt::KeepAspectRatio);
}

void HypercubicMap::movePolicemen()
{
	// Si on peut attraper le voleur, on l'attrape
	for(qint32 i=0; i<getPolicemenCount(); ++i)
	{
		if(distance(thiefPosition(), policemanPosition(i)) == 1)
		{
			policemanPosition(i) = thiefPosition();
			return;
		}
	}

	// Sinon, on se place dans une position gagnante
	for(qint32 i=0; i<getPolicemenCount(); ++i)
	{
		if(policemanPosition(i) != _targets[i])
		{
			// Déplacer le gendarme selon une des coordonnées différentes entre sa position et sa destination
			qint32 diff = policemanPosition(i) ^ _targets[i];
			qint32 coordDiff = 1;
			while(!(diff & coordDiff))
			{
				coordDiff <<= 1;
			}
			policemanPosition(i) ^= coordDiff;
			return;
		}
	}
}
