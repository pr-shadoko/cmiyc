#include "mainwindow.h"
#include "ui_mainwindow.h"

int const MainWindow::BENCHMARK_COUNT = 1000;

MainWindow::MainWindow(QWidget * parent) :
	QMainWindow(parent),
	_ui(new Ui::MainWindow),
	_gc(NULL),
	_benchmark(false),
	_benchmarkProgress(new QProgressBar)
{
	_ui->setupUi(this);

	_ui->_graphics_view->setResizeAnchor(QGraphicsView::AnchorViewCenter);
	_ui->_status_bar->addPermanentWidget(_benchmarkProgress);
	_benchmarkProgress->setVisible(false);
	_benchmarkProgress->setMinimum(0);
	_benchmarkProgress->setMaximum(BENCHMARK_COUNT - 1);

	connect(_ui->_action_nouveau, SIGNAL(triggered()), this, SLOT(newGame()));
}

MainWindow::~MainWindow()
{
	delete _ui;
	delete _gc;
	delete _benchmarkProgress;
}

void MainWindow::newGame()
{
	NewGameDialog * ngd = new NewGameDialog();
	if(QDialog::Rejected == ngd->exec())
	{
		return;
	}

	_benchmark = ngd->getBenchmark();
	_benchmarkMin = -1;
	_benchmarkMax = -1;
	_benchmarkMean = 0;
	if(_benchmark)
	{
		_benchmarkProgress->setValue(0);
		_benchmarkProgress->setVisible(true);
		_ui->_action_etape_suivante->setEnabled(false);
	}

	for(qint32 i=0; i<(_benchmark ? BENCHMARK_COUNT : 1); ++i)
	{
		if(NULL != _gc)
		{
			delete _gc;
		}

		_gc = new GameController(ngd->getSelectedMap(), ngd->getThiefIA());
		_ui->_graphics_view->setScene(_gc->getScene());
		_gc->update();

		connect(this, SIGNAL(resized()), _gc, SLOT(update()));
		connect(_gc, SIGNAL(gameOver()), this, SLOT(endGame()));
		if(_benchmark)
		{
			_gc->autoPlay();
		}
		else
		{
			_ui->_action_etape_suivante->setEnabled(true);
			connect(_ui->_action_etape_suivante, SIGNAL(triggered()), _gc, SLOT(nextStep()));
		}
	};
}

void MainWindow::endGame()
{
	if(_benchmark)
	{
		_benchmarkMin = (_benchmarkMin == -1) ? _gc->getStep() : qMin(_gc->getStep(), _benchmarkMin);
		_benchmarkMax = qMax(_gc->getStep(), _benchmarkMax);
		_benchmarkMean += _gc->getStep();
		qint32 bmStep = _benchmarkProgress->value() + 1;
		_benchmarkProgress->setValue(bmStep);
		if(bmStep == BENCHMARK_COUNT)
		{
			_benchmarkMean /= BENCHMARK_COUNT;
			_benchmarkProgress->setVisible(false);
			_ui->_action_etape_suivante->setEnabled(false);
			QMessageBox::information(this, tr("Game Over"), tr("Le voleur s'est fait attraper en %1 coups en moyenne.\nMin : %2\nMax : %3").arg(_benchmarkMean).arg(_benchmarkMin).arg(_benchmarkMax));
		}
	}
	else
	{
		_benchmarkMean = _gc->getStep();
		_ui->_action_etape_suivante->setEnabled(false);
		QMessageBox::information(this, tr("Game Over"), tr("Le voleur s'est fait attraper en %1 coups.").arg(_benchmarkMean));
	}
	delete _gc;
	_gc = NULL;
}

void MainWindow::resizeEvent(QResizeEvent *)
{
	emit resized();
}
